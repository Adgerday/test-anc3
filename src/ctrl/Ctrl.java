package ctrl;

import model.Tourney;
import model.Player;

public class Ctrl {
    
    private Tourney tourney;
    private Player player;
    
    public Ctrl(Tourney tourney) {
        this.tourney = tourney;
    }

   
    public void lineSelectionT(int numLine) {
        if (numLine >=0 && numLine < tourney.nbLines())
            tourney.select(numLine);
        else
            tourney.unselect();
    }

    
    public void addLineT() {
        tourney.addLine("");
    }

    
    public void updateSelectedLineT(String txt) {
        tourney.updateSelectedLine(txt);
    }
    
    
    public Ctrl(Player player) {
        this.player = player;
    }

    public void lineSelectionP(int numLine) {
        if (numLine >=0 && numLine < player.nbLinesP())
            player.selectP(numLine);
        else
            player.unselect();
    }
    

    public void addLineP() {
        player.addLineP("");
    }

    public void updateSelectedLineP(String txt) {
        player.updateSelectedLine(txt);
    }
    
}
