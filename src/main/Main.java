package main;

import ctrl.Ctrl;
import javafx.application.Application;
import javafx.stage.Stage;
import model.Tourney;
import model.Player;
import view.View;

public class Main extends Application {
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        Tourney tourney = new Tourney();
        Player player = new Player();
        Ctrl ctrl = new Ctrl(tourney);
        View view = new View(primaryStage, ctrl);
        tourney.addObserver(view);
//        player.addObserver(view);
        tourney.notif(Tourney.TypeNotif.INIT);
//        player.notif(Player.TypeNotifP.INIT);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
