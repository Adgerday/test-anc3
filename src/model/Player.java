package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class Player extends Observable {

    // faire une function TypeNotif pour les trois et faire InitP, InitM, InitT dedans pour les trois)
    public enum TypeNotifP {
        INIT, LINE_SELECTED, LINE_UNSELECTED, LINE_UPDATED, LINE_ADDED
    }

    private static final int MAX_WORD_LENGTH = 15;
    private final List<String> linesP = new ArrayList<>();
    private int numLineSelectedP = -1;

    public Player() {
        initData();
    }

    public String SelectedLineP() {
        return linesP.get(numLineSelectedP);
    }

    public int SelectedIndexP() {
        return numLineSelectedP;
    }

    public List<String> getLinesP() {
        return linesP;
    }

    public int nbLinesP() {
        return linesP.size();
    }

    public boolean addLineP(String line) {
        if (line.length() <= MAX_WORD_LENGTH) {
            linesP.add(line);
            numLineSelectedP = linesP.size() - 1;
            notif(TypeNotifP.LINE_ADDED);
            return true;
        }
        return false;
    }

    public void selectP(int index) {
        numLineSelectedP = index;
        notif(TypeNotifP.LINE_SELECTED);
    }

    public boolean updateSelectedLine(String txt) {
        if (txt.length() <= MAX_WORD_LENGTH) {
            linesP.set(numLineSelectedP, txt);
            notif(TypeNotifP.LINE_UPDATED);
            return true;
        }
        return false;
    }

    public void unselect() {
        numLineSelectedP = -1;
        notif(TypeNotifP.LINE_UNSELECTED);
    }

    public void notif(TypeNotifP typeNotifP) {
        setChanged();
        notifyObservers(typeNotifP);
    }

    private void initData() {
        addLineP("Adiboulefou");
        addLineP("Bernard");
        addLineP("Garou");
        addLineP("Loup");
        addLineP("Afleloup");
    }

}
