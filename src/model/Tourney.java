package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class Tourney extends Observable {
    //idée ? ne faire qu'un fichier model et mettre tout dedans
     public enum TypeNotif {
        INIT, INIT_P, LINE_SELECTED, LINE_SELECTED_P, LINE_UNSELECTED, LINE_UNSELECTED_P, LINE_UPDATED, LINE_UPDATED_P, LINE_ADDED, LINE_ADDED_P
    }

    private static final int MAX_WORD_LENGTH = 25;
    private final List<String> linesT = new ArrayList<>();
    private int numLineSelected = -1;

    public Tourney() {
        initData();
    }

    public String SelectedLine() {
        return linesT.get(numLineSelected);
    }

    public int SelectedIndex() {
        return numLineSelected;
    }

    public List<String> getLines() {
        return linesT;
    }

    public int nbLines() {
        return linesT.size();
    }

    public boolean addLine(String line) {
        if (line.length() <= MAX_WORD_LENGTH) {
            linesT.add(line);
            numLineSelected = linesT.size() - 1;
            notif(TypeNotif.LINE_ADDED);
            return true;
        }
        return false;
    }

    public void select(int index) {
        numLineSelected = index;
        notif(TypeNotif.LINE_SELECTED);
    }

    public boolean updateSelectedLine(String txt) {
        if (txt.length() <= MAX_WORD_LENGTH) {
            linesT.set(numLineSelected, txt);
            notif(TypeNotif.LINE_UPDATED);
            return true;
        }
        return false;
    }

    public void unselect() {
        numLineSelected = -1;
        notif(TypeNotif.LINE_UNSELECTED);
    }

    public void notif(TypeNotif typeNotif) {
        setChanged();
        notifyObservers(typeNotif);
    }

    private void initData() {
        addLine("Tourney 1");
        addLine("Tourney 2");
    }

}
