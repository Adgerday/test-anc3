package view;

import ctrl.Ctrl;
import java.util.Observable;
import java.util.Observer;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.Tourney;
import model.Player;

public class View extends VBox implements Observer {

    private static final int TEXTSIZE = 400, SPACING = 10;
    private final BorderPane root = new BorderPane();
    private final HBox editionZoneT = new HBox(), textZoneT = new HBox();
    private final HBox editionZoneP = new HBox(), textZoneP = new HBox();
    private final HBox editionZoneM = new HBox(), textZoneM = new HBox();
    private final TextField editLineT = new TextField(), editLineP = new TextField(), editLineM = new TextField();
    private final ListView<String> lvLinesT = new ListView<>(), lvLinesP = new ListView<>(), lvLinesM = new ListView<>();
    private final Label lbNbLinesT = new Label(), lbNbLinesP = new Label(), lbNbLinesM = new Label();
    private final Button btAddLineT = new Button(), btAddLineP = new Button(), btAddLineM = new Button();

    private final Ctrl ctrl;

    public View(Stage primaryStage, Ctrl ctrl) {
        this.ctrl = ctrl;
        configComponents();
        configListeners();
        Scene scene = new Scene(this, 600, 400);
        primaryStage.setTitle("Tourneys App");
        primaryStage.setScene(scene);
    }
    
    //toutes les config composants
    private void configComponents() {
        configEditZoneT();
        configEditZoneP();
        configEditZoneM();
        configTextZoneT();
        configTextZoneP();
        configTextZoneM();
        configWindow();
    }
    //config de la hbox edition zone qui recoit le textfield + bt
    private void configEditZoneT() {
        editionZoneT.setSpacing(SPACING);
        editLineT.setPrefWidth(TEXTSIZE);
        btAddLineT.setText("New Tourney");
        editionZoneT.getChildren().addAll(editLineT, btAddLineT);
    }
    
    private void configEditZoneP() {
        editionZoneP.setSpacing(SPACING);
        editLineP.setPrefWidth(TEXTSIZE);
        btAddLineP.setText("New Player");
        editionZoneP.getChildren().addAll(editLineP, btAddLineP);
    }
    private void configEditZoneM() {
        editionZoneM.setSpacing(SPACING);
        editLineM.setPrefWidth(TEXTSIZE);
        btAddLineM.setText("New Match");
        editionZoneM.getChildren().addAll(editLineM, btAddLineM);
    }
    //config de la hbox textzone qui recoit la list + le label
    private void configTextZoneT() {
        textZoneT.setSpacing(SPACING);
        lvLinesT.setPrefWidth(TEXTSIZE);
        textZoneT.getChildren().addAll(lvLinesT, lbNbLinesT);
    }
    private void configTextZoneP() {
        textZoneP.setSpacing(SPACING);
        lvLinesP.setPrefWidth(TEXTSIZE);
        textZoneP.getChildren().addAll(lvLinesP, lbNbLinesP);
    }
    private void configTextZoneM() {
        textZoneM.setSpacing(SPACING);
        lvLinesM.setPrefWidth(TEXTSIZE);
        textZoneM.getChildren().addAll(lvLinesM, lbNbLinesM);
    }
    
    //recoit les deux configs du dessus, les deux hbox
    private void configWindow() {
        this.setPadding(new Insets(SPACING));
        this.setSpacing(10);
        this.getChildren().addAll(editionZoneT, textZoneT, editionZoneP, textZoneP, editionZoneM, textZoneM);
    }
    //toutes les configs LISTENERS
    private void configListeners() {
        configListenersEditZoneT();
        configListenersEditZoneP();
        configListenersTextZoneT();
        configListenersTextZoneP();
    }
    //LISTENER TOURNEYS
    private void configListenersEditZoneT() {
        configListenerEditLineT();
        configListenerBtAddLineT();
    }

    private void configListenerEditLineT() {
        editLineT.setOnAction(e -> {
            if (editLineT.isEditable()) {
                ctrl.updateSelectedLineT(editLineT.getText());
            }
        });
    }

    private void configListenerBtAddLineT() {
        btAddLineT.setOnAction((ActionEvent event) -> {
            ctrl.addLineT();
        });
    }

    private void configListenersTextZoneT() {
        configFocusListenerT();
        configListenerSelectionLineT();
    }

    private void configFocusListenerT() {
        lvLinesT.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) 
                editLineT.requestFocus();
        });
        editLineT.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) 
                editLineT.selectAll();
        });
    }

    private void configListenerSelectionLineT() {
        getListViewModelT().selectedIndexProperty()
                .addListener(o -> {
                    ctrl.lineSelectionT(getListViewModelT().getSelectedIndex());
                });
    }
    // LISTENER PLAYER
    private void configListenersEditZoneP() {
        configListenerEditLineP();
        configListenerBtAddLineP();
    }

    private void configListenerEditLineP() {
        editLineP.setOnAction(e -> {
            if (editLineP.isEditable()) {
                ctrl.updateSelectedLineP(editLineP.getText());
            }
        });
    }

    private void configListenerBtAddLineP() {
        btAddLineP.setOnAction((ActionEvent event) -> {
            ctrl.addLineP();
        });
    }

    private void configListenersTextZoneP() {
        configFocusListenerP();
        configListenerSelectionLineP();
    }

    private void configFocusListenerP() {
        lvLinesP.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) 
                editLineP.requestFocus();
        });
        editLineP.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) 
                editLineP.selectAll();
        });
    }

    private void configListenerSelectionLineP() {
        getListViewModelP().selectedIndexProperty()
                .addListener(o -> {
                    ctrl.lineSelectionP(getListViewModelP().getSelectedIndex());
                });
    }

    @Override
    public void update(Observable o, Object arg) {
        Tourney tourney = (Tourney) o;
        Tourney.TypeNotif typeNotif = (Tourney.TypeNotif) arg;
        switch (typeNotif) {
            case INIT:
                lvLinesT.getItems().setAll(tourney.getLines());
                lbNbLinesT.setText("Number of tourneys : " + tourney.nbLines());
                setTextZoneEditable(false);
                break;
            case LINE_SELECTED:
                setTextZoneEditable(true);
                editLineT.setText(getListViewModelT().getSelectedItem());
                break;
            case LINE_UNSELECTED:
                setTextZoneEditable(false);
                getListViewModelT().select(-1);
                break;
            case LINE_UPDATED:
                lvLinesT.getItems().set(tourney.SelectedIndex(), tourney.SelectedLine());
                editLineT.setText("");
                setTextZoneEditable(false);
                getListViewModelT().select(-1);
                break;
            case LINE_ADDED:
                lvLinesT.getItems().add(tourney.SelectedLine());
                lbNbLinesT.setText("Number of tourneys : " + tourney.nbLines());
                getListViewModelT().select(tourney.SelectedIndex());
                setTextZoneEditable(true);
                break;
        }
//        Player player = (Player) o;
//        Player.TypeNotifP typeNotifP = (Player.TypeNotifP) arg;
//        switch (typeNotifP) {
//            case INIT:
//                lvLinesP.getItems().setAll(player.getLinesP());
//                lbNbLinesP.setText("Number of players : " + player.nbLinesP());
//                setTextZoneEditable(false);
//                break;
//            case LINE_SELECTED:
//                setTextZoneEditable(true);
//                editLineP.setText(getListViewModelP().getSelectedItem());
//                break;
//            case LINE_UNSELECTED:
//                setTextZoneEditable(false);
//                getListViewModelP().select(-1);
//                break;
//            case LINE_UPDATED:
//                lvLinesP.getItems().set(player.SelectedIndexP(), player.SelectedLineP());
//                editLineP.setText("");
//                setTextZoneEditable(false);
//                getListViewModelP().select(-1);
//                break;
//            case LINE_ADDED:
//                lvLinesP.getItems().add(player.SelectedLineP());
//                lbNbLinesP.setText("Number of players : " + player.nbLinesP());
//                getListViewModelP().select(player.SelectedIndexP());
//                setTextZoneEditable(true);
//                break;
//        }
    }
 
    private SelectionModel<String> getListViewModelT() {
        return lvLinesT.getSelectionModel();
    }
    private SelectionModel<String> getListViewModelP() {
        return lvLinesP.getSelectionModel();
    }
    private void setTextZoneEditable(boolean b) {
        editLineT.setEditable(b);
        editLineP.setEditable(b);
        btAddLineT.setDisable(b);
        btAddLineP.setDisable(b);
    }

}
